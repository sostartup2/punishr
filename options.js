function register_options(){
    chrome.storage.sync.get('userauth', function (result){
      var key = result.userauth;
      if(key){
        // Update database, sending authentication code
        alert("User registered. All good");

        var rest_api_url = "http://slackly.cloudapp.net:8000/api/";

        // set active charity
        $.ajax({
            type: 'POST',
            url : rest_api_url +'SetActiveCharity',
            data : {
                access_key: key,
                charity_id: $("#charityselect").val()
            },
            dataType : 'jsonp',
            success : function(data) {
              alert("great success user charity!");
            }

        });
        
        // set donation amount
        $.ajax({
            type: 'POST',
            url : rest_api_url + 'SetDonationAmount',
            data : {
                access_key: key,
                amount : $("#myselect").val()
            },
            dataType : 'jsonp',
            success : function(data) {
               alert("great success donation amount!");
            }
        });
        
        // set user blacklist
        $.ajax({
            type: 'POST',
            url : rest_api_url + 'SetUserBlacklist',
            data : {
                access_key: key,
                sites : ["facebook.com","reddit.com","friendsy.com","armorgames.com","addictinggames.com","pinterest.com","buzzfeed.com"]
            },
            dataType : 'jsonp',
            success : function(data) {
               alert("great success donation amount!");
            }
        });
      }else{
        alert("User not registered. Please go to registration");
      }
    });
}

// Sets chrome.storage key when
function setKey(key){
  var theValue = key;
  chrome.storage.sync.set({'value': theValue}, function(){
    // Notify that we saved.
    message('Settings saved');
  });
}

document.addEventListener('DOMContentLoaded', function(){
  var btn = document.getElementById("settings_btn");
  if (btn){
    btn.addEventListener("click", register_options);
  }else{
    alert("ERR");
  }
});
