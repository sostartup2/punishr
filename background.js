// Background js for Procrastinaid

/**
*   Listens for new url
**/
// chrome.tabs.onCreated.addListener(function(tabID, changeInfo, tab){
//     alert(HELLO);
// });

var badUrls = ["facebook", "yelp", "reddit"];


// Listens for bad url on activated tab:
chrome.tabs.onActivated.addListener(function(tabID, changeInfo, tab){
      chrome.tabs.query({active: true, currentWindow: true}, function(arrayOfTabs) {
        // get current tab:
         var activeTab = arrayOfTabs[0];
         var activeUrl = activeTab.url;

         // check if current url is in list of bad ones:
         for (var i = 0; i < badUrls.length; i++){
            if (activeUrl.includes(badUrls[i])){

                // display alert
                var top_msg = "You are on your restricted website: \n  ";
                var middle_msg = "\n You will donate $";
                var amount = "0.10";
                var bottom_msg = " to ";
                var charity = "United Way.\n";
                var total = "Total: $";

                

                chrome.storage.sync.get('donation', function(result){
                    var donationVal = 0;
                    if(result.donation){
                        donationVal = result.donation + 0.1;    
                    }else{
                        donationVal = 0.1;
                    }
                    
                    chrome.storage.sync.set({'donation': donationVal}, function(result){
                    });

                    alert(top_msg + activeUrl + middle_msg + amount + bottom_msg + charity + total + strip(donationVal));
                });

                // get user auth key to send to db
                chrome.storage.sync.get('userauth', function(result){
                    var key = result.userauth;
                    
                    var rest_api_url = "http://slackly.cloudapp.net:5000/api/";
                    // Update Balance:
                    $.ajax({
                        type: 'POST',
                        url : rest_api_url +'PayCharity',
                        data : {
                            access_key: key
                        },
                        dataType : 'jsonp',
                        success : function(data) {
                          alert("great success user charity!");
                        }

                    });


                });
            }
         }
    });
});


// Listens for bad url in non-new tab (for new loaded content):
var lastMove = 0;
chrome.webNavigation.onCompleted.addListener(function(){
    chrome.tabs.query({active: true, currentWindow: true}, function(arrayOfTabs) {
         // get current tab:
         var activeTab = arrayOfTabs[0];
         var activeUrl = activeTab.url;
         //var lastMove = 0;

         // Only care about events with enough time spacing:
         for (var i = 0; i < badUrls.length; i++){
             if (activeUrl.includes(badUrls[i])){
                    if(Date.now() - lastMove > 1500){
                        
                        // display alert
                        var top_msg = "You are on your restricted website: \n  ";
                        var middle_msg = "\n You will donate $";
                        var amount = "0.10";
                        var bottom_msg = " to ";
                        var charity = "United Way.\n";
                        var total = "Total: $";

                        

                        chrome.storage.sync.get('donation', function(result){
                            var donationVal = 0;
                            if(result.donation){
                                donationVal = result.donation + 0.1;
                            }else{
                                donationVal = 0.1;
                            }
                            
                            chrome.storage.sync.set({'donation': donationVal}, function(result){
                            });

                            alert(top_msg + activeUrl + middle_msg + amount + bottom_msg + charity + total + strip(donationVal));
                        });

                        // Get user auth key to send to db
                        chrome.storage.sync.get('userauth', function(result){
                            var key = result.userauth;
                            var rest_api_url = "http://slackly.cloudapp.net:5000/api/";
                            // Update Balance:
                            $.ajax({
                                type: 'POST',
                                url : rest_api_url +'PayCharity',
                                data : {
                                    access_key: key
                                },
                                dataType : 'jsonp',
                                success : function(data) {
                                  alert("great success user charity!");
                                }

                            });

                        });

                        lastMove = Date.now();
                    }
             }

         }

    });
});


function strip(number){
    return (parseFloat(number.toPrecision(3)));
}