from sqlalchemy import Column, ForeignKey, ForeignKeyConstraint, Integer, String, Date, DateTime, Text, Float, \
                        or_, and_, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound
import datetime as dt 

# Create an engine for the PostgreSQL database
db_url = 'postgres://Quinn@localhost/dadata'
engine = create_engine( db_url )

# Create the base class and bind its metadata to the engine
class_registry = {}
Base = declarative_base(class_registry=class_registry)
Base.metadata.bind = engine

# Create a session through the Session factory
Session = sessionmaker( bind=engine)
session = Session()

def get_session():
    return session


# Users table (configuration table, referencing customers and roles)
class User( Base): 
    __tablename__ = 'users'
    id = Column( Integer, primary_key = True)
    active_charity_id = Column( Integer, ForeignKey( 'charities.id'))

    customer_id = Column( Integer, nullable = False)
    first_name = Column( String( 100), nullable = False)
    last_name = Column( String( 100), nullable = False)
    email = Column( String( 250), nullable = False, unique = True)
    access_key = Column( String(100), nullable = False, unique = True)
    password = Column( String( 50), nullable = False)
    total_donation = Column( Float, nullable = False, default = 0.0 )
    balance = Column( Float, nullable = False, default = 0.0)
    limit = Column( Float, default = None)
    donation_size = Column( Float, nullable = False, default = 0.25)
    sites = Column (Text, nullable = False, default = "http://facebook.com")

def select_user_by_access_key(access_key):
    session = get_session()

    query = session.query(User).filter(User.access_key == access_key)

    return query.one()

class Charity (Base):
    __tablename__ = 'charities'
    id = Column( Integer, primary_key = True)
    name = Column( String(100), nullable = False)
    website = Column( String(250), nullable = False)

def select_charity_by_id(charity_id):
    session = get_session()

    query = session.query(Charity).filter(Charity.id == charity_id)

    return query.one()

def select_all_charities():
    session = get_session()
    query = session.query(Charity)

    return query.all()