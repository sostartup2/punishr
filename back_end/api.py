from flask import Flask, jsonify, abort, request


from models import User
import models, random, string

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'F34TF$($e34D'

API_PATH = '/api/'

from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

class CustomException(Exception):
    status_code = 404

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['status'] = 'failure'
        rv['message'] = self.message
        return rv

class InvalidUsage(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        CustomException.__init__(self, message=message, status_code=400, payload=payload)

class GatewayError(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        CustomException.__init__(self, message=message, status_code=502, payload=payload)

class DatabaseError(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        CustomException.__init__(self, message="database error: "+message, status_code=400, payload=payload)

class AuthenticationError(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        CustomException.__init__(self, message=message, status_code=401, payload=payload)

class InvalidAPI(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        # could use status code 410 Gone as well
        CustomException.__init__(self, message=message, status_code=404, payload=payload)

class UnsupportedAPI(CustomException):
    def __init__(self, message, status_code=None, payload=None):
        CustomException.__init__(self, message=message, status_code=501, payload=payload)

def request_get(keyword, default=None, type=None):
    if default==None and keyword not in request.args:
        raise InvalidUsage("Invalid usage: missing input {}".format(keyword))

    return request.args.get(keyword, default, type)

def request_post(keyword, default=None, type=None):
    if default==None and keyword not in request.form:
        raise InvalidUsage("Invalid usage: missing input {}".format(keyword))

    return request.form.get(keyword, default, type)

@app.route(API_PATH+'test', methods=['GET', 'POST'])
def test():
    return jsonify(status="HURRAY!")

@app.route(API_PATH+'CreateUser', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def createUser():
    customer_id = request_post('customer_id')
    first_name = request_post('first_name')
    last_name = request_post('last_name')
    email = request_post('email')
    password = request_post('password')
    active_charity_id = request_post('active_charity_id')

    access_key = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(30))

    session = models.get_session()
    user = User(first_name = first_name,\
        last_name = last_name,\
        customer_id = customer_id,\
        access_key = access_key,\
        email = email,\
        password = password,\
        active_charity_id = active_charity_id )

    session.add(user)
    session.commit()

    return jsonify(status='success', access_key=access_key)

@app.route(API_PATH+'login')
def login():
    pass

@app.route(API_PATH+'GetUser', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def getUser():
    access_key = request_get('access_key')

    user = models.select_user_by_access_key(access_key)
    charity = models.select_charity_by_id(user.active_charity_id)

    return jsonify(first_name=user.first_name, \
        last_name=user.last_name, \
        total_donation=user.total_donation, \
        balance=user.balance, \
        limit=user.limit, \
        donation_size=user.donation_size, \
        active_charity=charity.name)

@app.route(API_PATH+'SetUserBlacklist', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def setBlackList():
    access_key = request_post('access_key')
    session = models.get_session()
    user = models.select_user_by_access_key(access_key)
    sites = request_post('sites')

    site_string = ''
    for i in sites:
        site_string += ';'
        i.strip()
        site_string.append(i)

    user.sites = site_string
    session.commit()

    return jsonify(status='success')

@app.route(API_PATH+'GetUserBlacklist', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def getBlackList():
    access_key = request_get('access_key')

    user = models.select_user_by_access_key(access_key)
    sites = user.sites.split(',')

    return jsonify(sites=sites)

@app.route(API_PATH+'SetActiveCharity', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def setCharity():
    access_key = request_post('access_key')
    charity_id = request_post('charity_id')
    session = get_session()

    user = models.select_user_by_access_key(access_key)
    charity = models.select_charity_by_id(charity_id)

    if charity:
        user.active_charity_id = charity_id
        session.commit()
        return jsonify(status='success')
    else:
        session.commit()
        return jsonify(status='Charity not found.')

@app.route(API_PATH+'GetAvailableCharities', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def getCharities():
    charities = models.select_all_charities()

    names = [{'name':c.name, 'id':c.id} for c in charities]

    return jsonify(charities=names)

@app.route(API_PATH+'payCharity', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def payCharity():
    access_key = request_post('access_key')

    session = get_session()
    user = models.select_user_by_access_key(access_key)

    user.balance += donation_size
    user.total_donation += donation_size

    session.commit()

    return jsonify(status='success')

@app.route(API_PATH+'SetDonationAmount', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def setDonationAmount():
    access_key = request_post('access_key')
    amount = request_post('amount')

    session = get_session()
    user.donation_size = amount
    session.commit()

    return jsonify(status='success')

@app.route(API_PATH+'SetDonationLimit', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def setDonationLimit():
    access_key = request_post('access_key')
    limit = request_post('limit')

    session = get_session()
    user.limit = limit
    session.commit()

    return jsonify(status='success')

@app.route(API_PATH+'<path:action>', methods=['GET','POST', 'OPTIONS'])
@crossdomain(origin='*')
def not_supported(action):
    return jsonify( status = 'UNSUPPORTED PYTHON API [%s]'%(action) )

@app.errorhandler(CustomException)
@crossdomain(origin='*')
def handle_custom_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@app.errorhandler(404)
@crossdomain(origin='*')
def handle_not_found(error):
    response = jsonify({"error" : "Not found"})
    response.status_code = 404
    return response

@app.errorhandler(500)
@crossdomain(origin='*')
def handle_internal_error(error):
    response = jsonify({"error" : "Internal server error"})
    response.status_code = 500
    return response


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000, threaded=True) #port 5000 is used because port 80 has special uses elsewhere in the system










