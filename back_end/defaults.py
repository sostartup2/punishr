from models import Charity
import models

def add_default_charities():
    session = models.get_session()

    water = Charity(name = "water.org", website = "water.org")
    session.add(water)

    charity = Charity(name = "United Way", )
    session.commit()

if (__name__) == '__main__':
    add_default_charities()