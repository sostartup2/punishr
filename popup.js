// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


document.addEventListener('DOMContentLoaded', function() {
    var charity_text = "Currently donating to: "
    var charity_info = "Water.org"
    document.getElementById('charity').textContent = (charity_text + charity_info);

    var month_text = "The current month's donation is: $";
    
    var donation = 0;

    var total_text = "Total donated: $"

    chrome.storage.sync.get('donation', function(result){
        if(result.donation){
            donation = strip(result.donation);
        }

        var total = donation;
        document.getElementById('curr_month').textContent = (month_text + donation);
        document.getElementById('total_donated').textContent = (total_text + total);
    });
    

    //document.getElementById('curr_month').textContent = (month_text + donation);
    
    var last_pay_text = "Last payment date: "
    var last_date = "04/01/15"
    document.getElementById('last_pay').textContent = (last_pay_text + last_date);

});


function strip(number){
    return (parseFloat(number.toPrecision(3)));
}